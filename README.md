# linux-azure-test


### Build Docker Image

```
docker build -t azure-c-dev docker
```

### Run Docker Container

```
docker run --rm -ti -v $(pwd):/root/dev/ azure-c-dev
```
