#! /bin/sh

set -e

BIN=/usr/local/bin/mybin

if [ -n "${UID}" -a -n "${GID}" ] ; then
    echo "INFO: running binary as ${UID}:${GID}"
    sudo -u \#${UID} -g \#${GID} -- ${BIN} $@
else
   ${BIN} $@
fi
