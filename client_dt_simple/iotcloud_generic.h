#ifndef IOTCLOUD_GENERIC_HDR
#define IOTCLOUD_GENERIC_HDR

#define IOTCLOUD_GENERIC_CONNECTION_LEN     512
#define IOTCLOUD_GENERIC_METHOD_LEN         32

#include <pthread.h>

#include "iotcloud.h"

struct iotcloud_provider;

struct iotcloud {
    char connection[IOTCLOUD_GENERIC_CONNECTION_LEN];
    int connected;
    char *desired_state;
    struct iotcloud_provider *provider_data;
    void *user_ctx;
    iotcloud_event_cb user_cb;
    // XXX pthread_mutex_t lock; // XXX lock structure before rw operation cb/app
};

struct iotcloud_event_data {
    int status;
    char method[IOTCLOUD_GENERIC_METHOD_LEN];
    char *payload;
    size_t payload_len;
    char *response;
    size_t response_len;
};

int iotcloud_provider_init(struct iotcloud *handle);
int iotcloud_provider_provision(struct iotcloud *handle);
int iotcloud_provider_connect(struct iotcloud *handle);
int iotcloud_provider_disconnect(struct iotcloud *handle);
int iotcloud_provider_report_state(struct iotcloud *handle, const char *state);
int iotcloud_provider_request_state(struct iotcloud *handle);

int iotcloud_provider_get_lock(struct iotcloud *handle);

#endif
