#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "parson.h"
#include "iotcloud.h"


static const char* connection = "HostName=FIJI-IOT-HUB.azure-devices.net;DeviceId=test-container-01;SharedAccessKey=E8QfuvJUU2ePHBVKERby09UWiHD9Hl01nkzouj4b72s=";



void test_event_process_state_received_partial(struct iotcloud_event_data *data, void *ctx)
{
    char *payload = NULL;

    printf("%s run\n", __func__);

    iotcloud_event_data_get_payload(data, &payload);

    printf("%s: data payload [%s]\n", __func__, payload);

    return;
}


void test_event_process_state_received_complete(struct iotcloud_event_data *data, void *ctx)
{
    char *payload = NULL;

    printf("%s: run\n", __func__);

    iotcloud_event_data_get_payload(data, &payload);

    printf("%s: data payload [%s]\n", __func__, payload);

    return;
}


void test_event_process_state_reported_result(struct iotcloud_event_data *data, void *ctx)
{
    int status = 0;

    printf("%s: run\n", __func__);

    status = iotcloud_event_data_get_status(data);

    printf("%s: report status [%d]\n", __func__, status);

    return;
}


void test_event_process_method_call(struct iotcloud_event_data *data, void *ctx)
{
    int ret = 0;
    char *method_name = NULL;
    char *payload = NULL;

    printf("%s: run\n", __func__);

    iotcloud_event_data_get_method(data, &method_name);
    iotcloud_event_data_get_payload(data, &payload);

    printf("%s: method=[%s]\n", __func__, method_name);

    if (strcmp("ping", method_name) == 0) {
        if (payload != NULL) {
            printf("%s: payload=[%s]\n", __func__, payload);
        }
        iotcloud_event_data_set_status(data, 200);
        iotcloud_event_data_set_response(data, "\"pong\"");
    } else {
        iotcloud_event_data_set_status(data, 405);
    }

    return;
}


void test_event_cb(enum iotcloud_event_id id, struct iotcloud_event_data *data, void *ctx)
{
    printf("%s: event id %d\n", __func__, (int)id);

    switch (id) {
        case IOTCLOUD_EV_STATE_RECEIVED_PARTIAL:
            test_event_process_state_received_partial(data, ctx);
            break;
        case IOTCLOUD_EV_STATE_RECEIVED_COMPLETE:
            test_event_process_state_received_complete(data, ctx);
            break;
        case IOTCLOUD_EV_STATE_REPORTED_RESULT:
            test_event_process_state_reported_result(data, ctx);
            break;
        case IOTCLOUD_EV_METHOD_CALL:
            test_event_process_method_call(data, ctx);
            break;
        case IOTCLOUD_EV_CONNECTED:
        case IOTCLOUD_EV_DISCONNECTED:
        default:
            break;
    }

    return;
}


int main(int argc, char *argv[])
{
    struct iotcloud *handle = NULL;

    iotcloud_init(&handle, test_event_cb, NULL);

    iotcloud_connect(handle, connection);

    sleep(3);
    iotcloud_report_state(handle, "{\"foo\":\"bar\"}");

    sleep(3);
    iotcloud_request_state(handle);

    sleep(20);

    iotcloud_disconnect(handle);

    return 0;
}


// --------------------------------------------------------------------------------------------------------------------


#if 0
typedef struct MAKER_TAG
{
    char* makerName;
    char* style;
    int year;
} Maker;

typedef struct GEO_TAG
{
    double longitude;
    double latitude;
} Geo;

typedef struct CAR_STATE_TAG
{
    int32_t softwareVersion;        // reported property
    uint8_t reported_maxSpeed;      // reported property
    char* vanityPlate;              // reported property
} CarState;

typedef struct CAR_SETTINGS_TAG
{
    uint8_t desired_maxSpeed;       // desired property
    Geo location;                   // desired property
} CarSettings;

typedef struct CAR_TAG
{
    char* lastOilChangeDate;        // reported property
    char* changeOilReminder;        // desired property
    Maker maker;                    // reported property
    CarState state;                 // reported property
    CarSettings settings;           // desired property
} Car;
#endif

#if 0
//  Converts the Car object into a JSON blob with reported properties that is ready to be sent across the wire as a twin.
static char* serializeToJson(Car* car)
{
    char* result;

    JSON_Value* root_value = json_value_init_object();
    JSON_Object* root_object = json_value_get_object(root_value);

    // Only reported properties:
    (void)json_object_set_string(root_object, "lastOilChangeDate", car->lastOilChangeDate);
    (void)json_object_dotset_string(root_object, "maker.makerName", car->maker.makerName);
    (void)json_object_dotset_string(root_object, "maker.style", car->maker.style);
    (void)json_object_dotset_number(root_object, "maker.year", car->maker.year);
    (void)json_object_dotset_number(root_object, "state.reported_maxSpeed", car->state.reported_maxSpeed);
    (void)json_object_dotset_number(root_object, "state.softwareVersion", car->state.softwareVersion);
    (void)json_object_dotset_string(root_object, "state.vanityPlate", car->state.vanityPlate);

    result = json_serialize_to_string(root_value);

    json_value_free(root_value);

    return result;
}
#endif

#if 0
//  Converts the desired properties of the Device Twin JSON blob received from IoT Hub into a Car object.
static Car* parseFromJson(const char* json, DEVICE_TWIN_UPDATE_STATE update_state)
{
    Car* car = malloc(sizeof(Car));
    JSON_Value* root_value = NULL;
    JSON_Object* root_object = NULL;

    (void)memset(car, 0, sizeof(Car));

    root_value = json_parse_string(json);
    root_object = json_value_get_object(root_value);

    // Only desired properties:
    JSON_Value* changeOilReminder;
    JSON_Value* desired_maxSpeed;
    JSON_Value* latitude;
    JSON_Value* longitude;

    if (update_state == DEVICE_TWIN_UPDATE_COMPLETE)
    {
        changeOilReminder = json_object_dotget_value(root_object, "desired.changeOilReminder");
        desired_maxSpeed = json_object_dotget_value(root_object, "desired.settings.desired_maxSpeed");
        latitude = json_object_dotget_value(root_object, "desired.settings.location.latitude");
        longitude = json_object_dotget_value(root_object, "desired.settings.location.longitude");
    }
    else
    {
        changeOilReminder = json_object_dotget_value(root_object, "changeOilReminder");
        desired_maxSpeed = json_object_dotget_value(root_object, "settings.desired_maxSpeed");
        latitude = json_object_dotget_value(root_object, "settings.location.latitude");
        longitude = json_object_dotget_value(root_object, "settings.location.longitude");
    }

    if (changeOilReminder != NULL)
    {
        const char* data = json_value_get_string(changeOilReminder);

        if (data != NULL)
        {
            car->changeOilReminder = malloc(strlen(data) + 1);
            if (NULL != car->changeOilReminder)
            {
                (void)strcpy(car->changeOilReminder, data);
            }
        }
    }

    if (desired_maxSpeed != NULL)
    {
        car->settings.desired_maxSpeed = (uint8_t)json_value_get_number(desired_maxSpeed);
    }

    if (latitude != NULL)
    {
        car->settings.location.latitude = json_value_get_number(latitude);
    }

    if (longitude != NULL)
    {
        car->settings.location.longitude = json_value_get_number(longitude);
    }
    json_value_free(root_value);

    return car;
}
#endif

#if 0
static void deviceTwinCallback(DEVICE_TWIN_UPDATE_STATE update_state, const unsigned char* payLoad, size_t size, void* userContextCallback)
{
    (void)update_state;
    (void)size;

    printf(">> %s: start\n", __func__);
    printf(">> %s: payload=[%s]\n", __func__, payLoad);

    Car* oldCar = (Car*)userContextCallback;
    Car* newCar = parseFromJson((const char*)payLoad, update_state);

    if (NULL == newCar)
    {
        printf("ERROR: parseFromJson returned NULL\r\n");
    }
    else
    {
        if (newCar->changeOilReminder != NULL)
        {
            if ((oldCar->changeOilReminder != NULL) && (strcmp(oldCar->changeOilReminder, newCar->changeOilReminder) != 0))
            {
                free(oldCar->changeOilReminder);
            }

            if (oldCar->changeOilReminder == NULL)
            {
                printf("Received a new changeOilReminder = %s\n", newCar->changeOilReminder);
                if ( NULL != (oldCar->changeOilReminder = malloc(strlen(newCar->changeOilReminder) + 1)))
                {
                    (void)strcpy(oldCar->changeOilReminder, newCar->changeOilReminder);
                    free(newCar->changeOilReminder);
                }
            }
        }

        if (newCar->settings.desired_maxSpeed != 0)
        {
            if (newCar->settings.desired_maxSpeed != oldCar->settings.desired_maxSpeed)
            {
                printf("Received a new desired_maxSpeed = %" PRIu8 "\n", newCar->settings.desired_maxSpeed);
                oldCar->settings.desired_maxSpeed = newCar->settings.desired_maxSpeed;
            }
        }

        if (newCar->settings.location.latitude != 0)
        {
            if (newCar->settings.location.latitude != oldCar->settings.location.latitude)
            {
                printf("Received a new latitude = %f\n", newCar->settings.location.latitude);
                oldCar->settings.location.latitude = newCar->settings.location.latitude;
            }
        }

        if (newCar->settings.location.longitude != 0)
        {
            if (newCar->settings.location.longitude != oldCar->settings.location.longitude)
            {
                printf("Received a new longitude = %f\n", newCar->settings.location.longitude);
                oldCar->settings.location.longitude = newCar->settings.location.longitude;
            }
        }

        free(newCar);
    }

    printf(">> %s: end\n", __func__);
}
#endif

#if 0
Car car;
memset(&car, 0, sizeof(Car));
car.lastOilChangeDate = "2016";
car.maker.makerName = "Fabrikam";
car.maker.style = "sedan";
car.maker.year = 2014;
car.state.reported_maxSpeed = 100;
car.state.softwareVersion = 1;
car.state.vanityPlate = "1I1";

char* reportedProperties = serializeToJson(&car);
#endif
