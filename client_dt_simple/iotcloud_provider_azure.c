#ifdef IOTCLOUD_PROVIDER_AZURE


/*
 * Azure IOT Notes:
 *
 * - Azure DEVICE_TWIN_UPDATE_STATE values:
 *       DEVICE_TWIN_UPDATE_PARTIAL: a partial state json is received with value to be updated
 *       DEVICE_TWIN_UPDATE_COMPLETE: a full state json is received
 *
 * - Azure works with http status codes
 *   TODO translate codes to generic enum iotcloud_result IOTCLOUD_RESULT_OK/NOK/ERROR/...
 *
 * - Azure does not allow empty response to method callback, at least provide "null"
 *
 * - Azure IOT SDK for C spawns thread that manages the connction, etc.
 *   when disconnect is called, errors occur (unable to get mutex, memory leaks, ...)
 *
 * - Azure also provides the "embedded C SDK" which seems better code quality and organization
 *   Currently not usable as is because of limited ports for ESP32
 */


#include <stdlib.h>

#include "azure_macro_utils/macro_utils.h"
#include "azure_c_shared_utility/threadapi.h"
#include "azure_c_shared_utility/platform.h"
#include "iothub_device_client.h"
#include "iothub_client_options.h"
#include "iothub.h"
#include "iothub_message.h"
#include "iothubtransportmqtt.h"

#include "iotcloud_generic.h"


// TODO tracer
#include <stdio.h>
#define trace_printf(fmt, ...) \
    printf(fmt, ##__VA_ARGS__)


struct iotcloud_provider {
    IOTHUB_CLIENT_TRANSPORT_PROVIDER protocol;
    IOTHUB_DEVICE_CLIENT_HANDLE ctx;
};


static int deviceMethodCallback(const char *method_name, const unsigned char *payload, size_t size,
        unsigned char **response, size_t *response_size, void *ctx)
{
    int ret;
    struct iotcloud *handle = (struct iotcloud *) ctx;
    struct iotcloud_event_data data = {0};
    enum iotcloud_event_id ev = IOTCLOUD_EV_METHOD_CALL;

    trace_printf(">>>>>> %s: start\n", __func__);

    strcpy(data.method, method_name);

    if (payload != NULL) {
        ((char *) payload)[size] = '\0';
    }

    if (strcmp(payload, "null") != 0) {
        data.payload = (char *) payload;
        data.payload_len = size;
    }

    handle->user_cb(ev, &data, handle->user_ctx);

    ret = data.status;

    if (data.response == NULL) {
        // XXX empty response is not allowed by azure lib
        data.response_len = 4;
        data.response = (char *) malloc(data.response_len + 1);
        strcpy(data.response, "null");
    }

    *response_size = data.response_len;
    *response = data.response;

    trace_printf(">>>>>> %s: end\n", __func__);

    return ret;
}


static void reportedStateCallback(int status_code, void *ctx)
{
    trace_printf(">> %s: start\n", __func__);
    trace_printf(">> Device Twin reported properties update completed with result: %d\r\n", status_code);

    struct iotcloud *handle = (struct iotcloud *) ctx;
    struct iotcloud_event_data data = {0};
    enum iotcloud_event_id ev = IOTCLOUD_EV_STATE_REPORTED_RESULT;

    data.status = status_code;

    handle->user_cb(ev, &data, handle->user_ctx);

    trace_printf(">> %s: end\n", __func__);

    return;
}


int iotcloud_provider_init(struct iotcloud *handle)
{
    int ret = 0;
    struct iotcloud_provider *data = NULL;

    ret = IoTHub_Init();
    if (ret != 0) {
        // error init
        ret = -1;
        goto exit_iotcloud_provider_init;
    }

    data = calloc(1, sizeof(struct iotcloud_provider));
    if (data == NULL) {
        // error malloc
        ret = -1;
        goto exit_iotcloud_provider_init;
    }

    handle->provider_data = data;
    ret = 0;

exit_iotcloud_provider_init:
    return ret;
}


int iotcloud_provider_provision(struct iotcloud *handle)
{
    // TODO
    return 0;
}


#if 0
int iotcloud_provider_deinit(struct iotcloud *handle)
{
    // TODO
    //IoTHub_Deinit();
    return 0;
}
#endif


static void deviceTwinCallback(DEVICE_TWIN_UPDATE_STATE update_state, const unsigned char *payload,
        size_t size, void *ctx)
{
    struct iotcloud *handle = (struct iotcloud *) ctx;
    struct iotcloud_event_data data = {0};
    enum iotcloud_event_id ev = IOTCLOUD_EV_NONE;

    if (update_state == DEVICE_TWIN_UPDATE_PARTIAL) {
        ev = IOTCLOUD_EV_STATE_RECEIVED_PARTIAL;
    } else if (update_state == DEVICE_TWIN_UPDATE_COMPLETE) {
        ev = IOTCLOUD_EV_STATE_RECEIVED_COMPLETE;
    } else {
        // XXX error in update_state
    }

    data.payload = (char *) payload;
    data.payload_len = size;
    data.payload[size] = '\0';

    handle->user_cb(ev, &data, handle->user_ctx);

    return;
}


int iotcloud_provider_connect(struct iotcloud *handle)
{
    int ret = 0;
    struct iotcloud_provider *data = handle->provider_data;

    data->protocol = MQTT_Protocol;
    data->ctx = IoTHubDeviceClient_CreateFromConnectionString(handle->connection, data->protocol);
    if (data->ctx == NULL) {
        // error create connection
        ret = -1;
        goto exit_iotcloud_provider_connect;
    }

    // XXX
    bool traceOn = true;
    IoTHubDeviceClient_SetOption(data->ctx, OPTION_LOG_TRACE, &traceOn);

    // XXX
    bool urlEncodeOn = true;
    IoTHubDeviceClient_SetOption(data->ctx, OPTION_AUTO_URL_ENCODE_DECODE, &urlEncodeOn);

    IoTHubDeviceClient_SetDeviceTwinCallback(data->ctx, deviceTwinCallback, handle);
    IoTHubDeviceClient_SetDeviceMethodCallback(data->ctx, deviceMethodCallback, handle);

    ret = 0;

exit_iotcloud_provider_connect:
    return ret;
}


int iotcloud_provider_disconnect(struct iotcloud *handle)
{
    // XXX errors in other thread when called
    IoTHubDeviceClient_Destroy(handle->provider_data->ctx);
    return 0;
}


int iotcloud_provider_report_state(struct iotcloud *handle, const char *state)
{
    int ret = 0;
    IOTHUB_CLIENT_RESULT result = 0;

    result = IoTHubDeviceClient_SendReportedState(handle->provider_data->ctx, state, strlen(state),
            reportedStateCallback, handle);
    if (result != IOTHUB_CLIENT_OK) {
        ret = -1;
        trace_printf(">> ERROR IoTHubDeviceClient_SendReportedState");
    }

    return ret;
}


int iotcloud_provider_request_state(struct iotcloud *handle)
{
    int ret = 0;
    IOTHUB_CLIENT_RESULT result = 0;

    result = IoTHubDeviceClient_GetTwinAsync(handle->provider_data->ctx, deviceTwinCallback, handle);
    if (result != IOTHUB_CLIENT_OK) {
        ret = -1;
        trace_printf(">> ERROR IoTHubDeviceClient_GetTwinAsync");
    }

    return ret;
}


int iotcloud_provider_get_lock(struct iotcloud *handle)
{
    return -1;
}


#endif
