#ifndef IOTCLOUD_HDR
#define IOTCLOUD_HDR

struct iotcloud;
struct iotcloud_event_data;

enum iotcloud_event_id {
    IOTCLOUD_EV_NONE,
    IOTCLOUD_EV_CONNECTED,
    IOTCLOUD_EV_DISCONNECTED,
    IOTCLOUD_EV_STATE_RECEIVED_PARTIAL,
    IOTCLOUD_EV_STATE_RECEIVED_COMPLETE,
    IOTCLOUD_EV_STATE_REPORTED_RESULT,
    IOTCLOUD_EV_METHOD_CALL,
    IOTCLOUD_EV_ERROR
};

typedef void (*iotcloud_event_cb)(enum iotcloud_event_id id, struct iotcloud_event_data *data, void *ctx);

int iotcloud_init(struct iotcloud **handle, iotcloud_event_cb cb, void *ctx);
int iotcloud_provision(struct iotcloud *handle, const char *cloud_id);
int iotcloud_connect(struct iotcloud *handle, const char *connection);
int iotcloud_disconnect(struct iotcloud *handle);
int iotcloud_report_state(struct iotcloud *handle, const char *reported_state);
int iotcloud_request_state(struct iotcloud *handle);

int iotcloud_event_data_get_status(struct iotcloud_event_data *data);
int iotcloud_event_data_set_status(struct iotcloud_event_data *data, int status);
int iotcloud_event_data_get_method(struct iotcloud_event_data *data, char **method);
int iotcloud_event_data_get_payload(struct iotcloud_event_data *data, char **payload);
int iotcloud_event_data_set_response(struct iotcloud_event_data *data, char *response);

#endif
