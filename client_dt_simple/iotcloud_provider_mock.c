#ifdef IOTCLOUD_PROVIDER_MOCK

#include <stdlib.h>

#include "iotcloud_generic.h"


// TODO tracer
#include <stdio.h>
#define trace_printf(fmt, ...) \
    printf(fmt, ##__VA_ARGS__)


struct iotcloud_provider {
    int id;
};

static struct iotcloud_provider ctx;


int iotcloud_provider_init(struct iotcloud *handle)
{
    ctx.id = 0;
    handle->provider_data = &ctx;
    return 0;
}


int iotcloud_provider_provision(struct iotcloud *handle)
{
    // TODO
    return 0;
}


#if 0
int iotcloud_provider_deinit(struct iotcloud *handle)
{
    // TODO
    return 0;
}
#endif


int iotcloud_provider_connect(struct iotcloud *handle)
{
    // TODO
    return 0;
}


int iotcloud_provider_disconnect(struct iotcloud *handle)
{
    // TODO
    return 0;
}


int iotcloud_provider_report_state(struct iotcloud *handle, const char *state)
{
    int ret = 0;

    // XXX TODO

    return ret;
}


int iotcloud_provider_request_state(struct iotcloud *handle)
{
    int ret = 0;

    // XXX TODO

    return ret;
}


int iotcloud_provider_get_lock(struct iotcloud *handle)
{
    return -1;
}


#endif
