/*
 *      list.h
 *      Library routines for managing doubly linked lists.
 *
 *      Copyright (c) 2016-2018 Philippe Mariman
 */

#ifndef LIST_HDR
#define LIST_HDR

#include <stddef.h>

#define list_foreach(list, lp) \
        for (lp = (list)->next; lp && (lp != (list)); lp = lp->next)

#define list_foreach_safe(list, lp, tmp) \
        for (lp = (list)->next; tmp = lp ? lp->next : NULL, lp && (lp != (list)); lp = tmp)

#define list_container(struct_type, struct_member, val) \
        ((struct_type *)(((char *)(val))-offsetof(struct_type,struct_member)))


struct list_node {
        struct list_node *next;
        struct list_node *prev;
};


static inline int __attribute__ ((always_inline)) list_init(struct list_node *el)
{
        if (el == NULL)
                return -1;
        el->next = el;
        el->prev = el;
        return 0;
}


static inline int __attribute__ ((always_inline)) list_delete(struct list_node *el)
{
        if (el == NULL)
                return -1;
        if (el->prev == el)
                goto exit_list_delete;
        if (el->prev == NULL)
                goto exit_list_delete;

        el->next->prev = el->prev;
        el->prev->next = el->next;

exit_list_delete:
        return list_init(el);
}


static inline int __attribute__ ((always_inline)) list_add(struct list_node *list, struct list_node *el)
{
        if (list == NULL || el == NULL)
                return -1;
        if (list->prev == NULL || list->next == NULL)
                list_init(list);
        list_delete(el);

        el->prev = list->prev;
        el->next = list;
        el->next->prev = el;
        el->prev->next = el;
        return 0;
}


static inline int __attribute__ ((always_inline)) list_is_empty(struct list_node *list)
{
        if (list == NULL)
                return -1;
        if (list->next == NULL)
                return 1;
        if (list->next && (list->next == list))
                return 1;
        else
                return 0;
}


static inline int __attribute__ ((always_inline)) list_get_first(struct list_node *list, struct list_node **el)
{
        if (list == NULL || el == NULL)
                return -1;
        if (!list_is_empty(list)) {
                *el = list->next;
                return 0;
        } else {
                return -1;
        }
}


static inline int __attribute__ ((always_inline)) list_get_last(struct list_node *list, struct list_node **el)
{
        if (list == NULL || el == NULL)
                return -1;
        if (!list_is_empty(list)) {
                *el = list->prev;
                return 0;
        } else {
                return -1;
        }
}


static inline int __attribute__ ((always_inline)) list_len(struct list_node *list)
{
        int i = 0;
        struct list_node *el;
        if (list == NULL)
                return -1;
        list_foreach(list,el)
                i++;
        return i;
}


typedef void (*list_cb_print)(struct list_node *el);

static inline int __attribute__ ((always_inline)) list_print(struct list_node *list, list_cb_print funcp)
{
        int i = 0;
        struct list_node *el = NULL;
        list_foreach(list,el) {
                funcp(el);
                i++;
        }
        return i;
}

#endif
