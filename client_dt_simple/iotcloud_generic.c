#include <stdlib.h>
#include <string.h>

#include "iotcloud_generic.h"


int iotcloud_init(struct iotcloud **handle, iotcloud_event_cb cb, void *ctx)
{
    int ret = 0;

    if (handle == NULL || cb == NULL) {
        // error params
        ret = -1;
        goto exit_iotcloud_init;
    }

    *handle = calloc(1, sizeof(struct iotcloud));
    if (*handle == NULL) {
        // error malloc
        ret = -1;
        goto exit_iotcloud_init;
    }

    (*handle)->user_cb = cb;
    (*handle)->user_ctx = ctx;

    // TODO call specific init
    ret = iotcloud_provider_init(*handle);

exit_iotcloud_init:
    return ret;
}


// TODO
int iotcloud_provision(struct iotcloud *handle, const char *cloud_id)
{
    return -1;
}


int iotcloud_connect(struct iotcloud *handle, const char *connection)
{
    int ret = 0;

    if (handle == NULL || connection == NULL) {
        // error params
        ret = -1;
        goto exit_iotcloud_connect;
    }

    if (strlen(connection) >= IOTCLOUD_GENERIC_CONNECTION_LEN) {
        // error len connection
        ret = -1;
        goto exit_iotcloud_connect;
    }

    strcpy(handle->connection, connection);

    // TODO call specific connect
    ret = iotcloud_provider_connect(handle);

exit_iotcloud_connect:
    return ret;
}


int iotcloud_disconnect(struct iotcloud *handle)
{
    int ret = 0;

    if (handle == NULL) {
        // error params
        ret = -1;
        goto exit_iotcloud_disconnect;
    }

    // TODO call specific disconnect
    ret = iotcloud_provider_disconnect(handle);
    if (ret != 0) {
        // error provider disconnects
        ret = -1;
        goto exit_iotcloud_disconnect;
        // XXX continue?
    }

    if (handle->desired_state != NULL) {
        free(handle->desired_state);
        handle->desired_state = NULL;
    }

    // TODO reset handle
    ret = iotcloud_provider_disconnect(handle);

    ret = 0;

exit_iotcloud_disconnect:
    return ret;
}


int iotcloud_report_state(struct iotcloud *handle, const char *reported_state)
{
    int ret = 0;

    if (handle == NULL || reported_state == NULL) {
        // error params
        ret = -1;
        goto exit_iotcloud_report_state;
    }

    if (strlen(reported_state) <= 0) {
        // error len reported_state
        ret = -1;
        goto exit_iotcloud_report_state;
    }

    // TODO call specific report_state
    ret = iotcloud_provider_report_state(handle, reported_state);

exit_iotcloud_report_state:
    return ret;
}


int iotcloud_request_state(struct iotcloud *handle)
{
    int ret = 0;

    if (handle == NULL) {
        // error params
        ret = -1;
        goto exit_iotcloud_request_state;
    }

    // TODO call specific get_desired_state
    ret = iotcloud_provider_request_state(handle);

exit_iotcloud_request_state:
    return ret;
}


int iotcloud_event_data_get_status(struct iotcloud_event_data *data)
{
    int ret = 0;

    if (data == NULL) {
        // error params
        ret = -1;
        goto exit_iotcloud_event_data_get_status;
    }

    ret = data->status;

exit_iotcloud_event_data_get_status:
    return ret;
}


int iotcloud_event_data_set_status(struct iotcloud_event_data *data, int status)
{
    int ret = 0;

    if (data == NULL) {
        // error params
        ret = -1;
        goto exit_iotcloud_event_data_set_status;
    }

    data->status = status;

exit_iotcloud_event_data_set_status:
    return ret;
}


int iotcloud_event_data_get_method(struct iotcloud_event_data *data, char **method)
{
    int ret = 0;
    int len = 0;

    if (data == NULL || method == NULL) {
        // error params
        ret = -1;
        goto exit_iotcloud_event_data_get_method;
    }

    len = strlen(data->method);

    if (len == 0) {
        ret = 0;
        *method = NULL;
    } else {
        ret = len;
        *method = data->method;
    }

exit_iotcloud_event_data_get_method:
    return ret;
}


int iotcloud_event_data_get_payload(struct iotcloud_event_data *data, char **payload)
{
    int ret = 0;

    if (data == NULL || payload == NULL) {
        // error params
        ret = -1;
        goto exit_iotcloud_event_data_get_payload;
    }

    if (data->payload == NULL || data->payload_len == 0) {
        ret = 0;
        *payload = NULL;
    } else {
        ret = data->payload_len;
        *payload = data->payload;
    }

exit_iotcloud_event_data_get_payload:
    return ret;
}


int iotcloud_event_data_set_response(struct iotcloud_event_data *data, char *response)
{
    int ret = 0;
    int len = 0;

    if (data == NULL || response == NULL) {
        // error params
        ret = -1;
        goto exit_iotcloud_event_data_set_response;
    }

    len = strlen(response);
    if (len <= 0) {
        // error response
        ret = -1;
        goto exit_iotcloud_event_data_set_response;
    }

    if (data->response != NULL) {
        // error data response already set
        ret = -1;
        goto exit_iotcloud_event_data_set_response;
    }

    data->response = (char *) malloc(len + 1);
    if (data->response == NULL) {
        // error malloc
        ret = -1;
        goto exit_iotcloud_event_data_set_response;
    }

    strcpy(data->response, response);
    data->response_len = len;

exit_iotcloud_event_data_set_response:
    return ret;
}
